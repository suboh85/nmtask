﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    /*
     * Class that models the information that we want fro meach entry in the
     * results of the API call. In this case, I was only interested in the name
     * of each Star Wars character.
     */
    public class CharacterModel
    {
        public string name { get; set; }
    }
}
