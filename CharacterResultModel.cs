﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    /*
     * Class that models the information we wanted from the API call. In this
     * case, I was only interested in the results list within the API call.
     */
    public class CharacterResultModel
    {
        public CharacterModel[] results { get; set; }
    }
}
