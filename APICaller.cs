﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    /*
     * Static class to call API's. It is a pretty simple lightweight class.
     * It could be abstracted and given more parameters to be generalized
     * a lot more, but this works fine.
     */
    public static class APICaller
    {
        public static HttpClient ApiClient { get; set; }

        /*
         * Initializer for the APICaller. It will only be called once upon
         * startup.
         */
        public static void InitializeClient()
        {
            ApiClient = new HttpClient();
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /*
         * Method that asynchronously calls the API to get the results. It
         * will then 'store' the results in the CharacterResultsModel class.
         * If nothing was returned, it will throw an exception.
         */
        public static async Task<CharacterResultModel> LoadCharacterInformation()
        {
            using (HttpResponseMessage response = await ApiClient.GetAsync("https://swapi.co/api/people/"))
            {
                if (response.IsSuccessStatusCode)
                {
                    CharacterResultModel characters = await response.Content.ReadAsAsync<CharacterResultModel>();

                    return characters;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

    }
}
